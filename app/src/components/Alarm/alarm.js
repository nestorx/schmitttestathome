import firebaseApp from '../firebaseInit'

export default {
  name: 'Alarm',
  data () {
    return {
      alarmActivated: null
    }
  },
  created () {
    firebaseApp.auth().onAuthStateChanged((user) => {
      if (user == null) {
        console.log('User ist nicht angemeldet')
        this.$router.push({path: './auth', query: { sourceuri: './alarm' }})
      } else {
        this.authorising(user.email, this.getAlarmState)
      }
    })
  },
  methods: {
    authorising: function (userEmail, callback) {
      var result
      var collectionUsers = firebaseApp.firestore().collection('users')
      collectionUsers.doc(userEmail).get().then(function (doc) {
        if (doc.exists) {
          // console.log('Document data:', doc.data())
          var user = doc.data()
          // console.log('User data:', user.email)
          callback()
          result = user.email
        } else {
          // doc.data() will be undefined in this case
          console.log('No such document!')
        }
      }).catch(function (error) {
        console.log('Error getting document:', error)
      })
      return result
    },
    getAlarmState: function () {
      var _this = this
      firebaseApp.firestore().collection('jobs').doc('wuxuVj1CdThrdQIvfKLw')
        .get()
        .then(function (doc) {
          if (doc.exists) {
            console.log(' Dokument existiert ' + doc.id)
            var tempDoc = doc.data()
            console.log(' doc ' + tempDoc.jobPayload)
            if (tempDoc.jobPayload === 'activated') {
              _this.alarmActivated = true
            } else {
              _this.alarmActivated = false
            }
          } else {
            console.log('No such document!')
          }
        }).catch(function (error) {
          console.log('Errort getting document: ', error)
        })
    },
    activatAlarm: function (event) {
      console.log('aktiviert')
      this.writeAlarmStatusToFirestore('activated')
      this.writeAlarmToFalse()
      this.alarmActivated = true
    },
    deactivatAlarm: function (event) {
      console.log('deaktiviert')
      this.writeAlarmStatusToFirestore('inactive')
      this.alarmActivated = false
    },
    writeAlarmStatusToFirestore: function (status) {
      firebaseApp.firestore().collection('jobs').doc('wuxuVj1CdThrdQIvfKLw').set({
        jobListStatus: 'created',
        jobType: 'SetAlarmStatus',
        jobPayload: status,
        resultPayload: status
      })
        .then(function () {
          console.log('Document successfully written!')
        })
        .catch(function (error) {
          console.error('Error writing document: ', error)
        })
    },
    writeAlarmToFalse: function () {
      firebaseApp.firestore().collection('alarms').doc('0THNhs514sx0o1uqACLe').set({
        alarm: false,
        deviceid: ''
      })
        .then(function () {
          console.log('Alarm Trigger auf false gesendet')
        })
        .catch(function (error) {
          console.error('Error writing document: ', error)
        })
    }

  }
}
