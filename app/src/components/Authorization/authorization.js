import firebaseApp from '../firebaseInit'

export default async function isAuthorized (userEmail) {
  var userFromDB = await getUserFromDatabase(userEmail)
  console.log('userFromDB = ' + userFromDB)
  if (userFromDB == null) {
    console.log('Benutzer ist NICHT authorisiert')
    return false
  } else {
    console.log('Benutzer ist authorisiert')
    return true
  }
}
// return is null if the user does not exist in the database
async function getUserFromDatabase (userEmail) {
  var result = null
  var collectionUsers = firebaseApp.firestore().collection('users')
  await collectionUsers.doc(userEmail).get().then(function (doc) {
    if (doc.exists) {
      // console.log('Document data:', doc.data())
      var user = doc.data()
      // console.log('User data:', user.email)
      result = user.email
    } else {
      // doc.data() will be undefined in this case
      console.log('No such document!')
    }
  }).catch(function (error) {
    console.log('Error getting document:', error)
  })
  return result
}
