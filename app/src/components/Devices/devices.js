import firebaseApp from '../firebaseInit'

export default {
  name: 'Devices',
  data () {
    return {
      devices: [],
      authorized: false
    }
  },
  created () {
    firebaseApp.auth().onAuthStateChanged((user) => {
      if (user == null) {
        console.log('User ist nicht angemeldet')
        this.$router.push({
          path: './auth',
          query: {
            sourceuri: './devices'
          }
        })
      } else {
        this.authorising(user.email, this.getDevices)
        // console.log('Devices ' + this.devices)
      }
    })
  },
  methods: {
    authorising: function (userEmail, callback) {
      var result
      var collectionUsers = firebaseApp.firestore().collection('users')
      collectionUsers.doc(userEmail).get().then(function (doc) {
        if (doc.exists) {
          // console.log('Document data:', doc.data())
          var user = doc.data()
          // console.log('User data:', user.email)
          callback()
          result = user.email
        } else {
          // doc.data() will be undefined in this case
          console.log('No such document!')
        }
      }).catch(function (error) {
        console.log('Error getting document:', error)
      })
      return result
    },
    getDevices: function () {
      var resultDevices = []
      // console.log('hole devices')
      firebaseApp.firestore().collection('devices').get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          // console.log('Document  data:', doc.data())

          var tempDocData = doc.data()
          var tempItemData = getItemsforDevice(doc.id)

          let data = {
            'id': doc.id,
            'name': tempDocData.name,
            'items': tempItemData
          }

          resultDevices.push(data)
        })
      })
      this.devices = resultDevices
      // console.log('DEVICES in der Funktion ' + resultDevices + ' !')
    }
  }
}

function getItemsforDevice (deviceID) {
  // console.log("getItemfor Device "+ deviceID);

  var items = []

  firebaseApp.firestore().collection('items').where('DeviceId', '==', deviceID)
    .get()
    .then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
      //  console.log('Document Items data:', doc.data())
        var tempItemID = doc.id
        var tempDocData = doc.data()
        tempDocData.itemID = tempItemID
        // tempDocData.jobID = newGuid()
        tempDocData.jobID = tempDocData.itemID
        // console.log('ID für Item ' + tempDocData.jobID)

        // inspectTheJob(tempDocData.jobID, doc.id, tempDocData.link)
        registerObserver(tempDocData.jobID, doc.id, tempDocData.link)
        items.push(tempDocData)
      })
    })
  return items
}

function registerObserver (jobID, docID, link) {
  // console.log('ID ' + jobID)
  firebaseApp.firestore().collection('jobs').doc('' + jobID)
    .get()
    .then(function (doc) {
      if (doc.exists) {
        var changedData = doc.data()
        var resultPayload = changedData.resultPayload
        var payloadObj = JSON.parse(resultPayload)
        var value = JSON.stringify(payloadObj.state)
        document.getElementById(jobID).innerHTML = '' + value
        inspectTheJob(jobID)
        // console.log('Document data:', doc.data())
      } else {
        // doc.data() will be undefined in this case
        console.log('No such document!')
        createAndInspectTheJob(jobID, docID, link)
      }
    }).catch(function (error) {
      console.log('Error getting document:', error)
    })
}

function inspectTheJob (jobID) {
  firebaseApp.firestore().collection('jobs').doc('' + jobID)
    .get()
    .then(function () {
      // console.log('Document successfully written!')
      firebaseApp.firestore().collection('jobs').doc('' + jobID)
        .onSnapshot(function (doc) {
          var changedData = doc.data()
          var resultPayload = changedData.resultPayload
          var payloadObj = JSON.parse(resultPayload)
          var value = JSON.stringify(payloadObj.state)
          var timestampValue = JSON.stringify(changedData.timestamp)
          // console.log(value)
          document.getElementById(jobID).innerHTML = '' + value
          document.getElementById('timestamp').innerHTML = '' + timestampValue
          // unsubscribeJob(jobId)
        })
    })
    .catch(function (error) {
      console.error('InspectTheJob Error Reading document: ', error)
    })
}

function createAndInspectTheJob (jobId, itemId, jobURL) {
  firebaseApp.firestore().collection('jobs').doc('' + jobId).set({
    id: jobId,
    jobListStatus: 'created',
    jobType: 'GetStatus',
    jobURL: jobURL,
    resultPayload: '{"state":"wird ermittelt"}'
  })
    .then(function () {
      // console.log('Document successfully written!')
      firebaseApp.firestore().collection('jobs').doc('' + jobId)
        .onSnapshot(function (doc) {
          var changedData = doc.data()
          var resultPayload = changedData.resultPayload
          var payloadObj = JSON.parse(resultPayload)
          var value = JSON.stringify(payloadObj.state)
          // console.log(value)
          document.getElementById(jobId).innerHTML = '' + value
          // unsubscribeJob(jobId)
        })
    })
    .catch(function (error) {
      console.error('Error writing document: ', error)
    })
  setTriggerActiveForOpenhabAgent()
}

function setTriggerActiveForOpenhabAgent () {
  firebaseApp.firestore().collection('messagetrigger').doc('openhabAgent').set({
    trigger: 'active'
  })
}

/**
function newGuid () {
  var sGuid = ''
  for (var i = 0; i < 32; i++) {
    sGuid += Math.floor(Math.random() * 0xF).toString(0xF)
  }
  return sGuid
}
**/
