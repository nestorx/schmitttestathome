import firebaseApp from '../firebaseInit'

const firestore = firebaseApp.firestore()

export default {
  data () {
    return {
      forecastList: []

    }
  },
  created () {
    firebaseApp.auth().onAuthStateChanged((user) => {
      if (user == null) {
        console.log('User ist nicht angemeldet')
        this.$router.push({
          path: './auth',
          query: {
            sourceuri: './weather'
          }
        })
      } else {
        this.authorising(user.email, this.getWeatherInfos)
        // console.log('Devices ' + this.devices)
      }
    })
  },
  methods: {

    authorising: function (userEmail, callback) {
      var result

      var collectionUsers = firestore.collection('users')
      collectionUsers.doc(userEmail).get().then(function (doc) {
        if (doc.exists) {
          // console.log('Document data:', doc.data())
          var user = doc.data()
          // console.log('User data:', user.email)
          callback()
          result = user.email
        } else {
          // doc.data() will be undefined in this case
          console.log('No such document!')
        }
      }).catch(function (error) {
        console.log('Error getting document:', error)
      })
      return result
    },
    getWeatherInfos: function () {
      var itemArray = ['weatherAtAhaus', 'MultisensorBalkon_SensorRelativeHumidity', 'MultisensorBalkon_SensorTemperature', 'MultisensorBalkon_SensorLuminance', 'ZWaveNode3ZW0744InOneMultiSensorG5_SensorTemperature_3', 'ZWaveNode3ZW0744InOneMultiSensorG5_SensorLuminance', 'ZWaveNode3ZW0744InOneMultiSensorG5_SensorRelativeHumidity']
      console.log('starte getWeatherInfos')
      firestore.collection('items').where('ItemID', 'in', itemArray)
        .get()
        .then((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            // console.log('Document Items data:', doc.data())
            var tempItemID = doc.id
            var tempDocData = doc.data()
            if (tempItemID.includes('weatherAtAhaus')) {
              console.log('Weather Data If')
              observerForItemWeatherAtAhaus()
              putDataFromWeatherAtAhausToHTMLSide(tempDocData)
            } else {
              // openhab Item
              console.log('else' + tempItemID)
              var state = tempDocData.State
              state = state.replace('?C', '')
              document.getElementById(tempDocData.ItemID).innerHTML = '' + state
              observerForItem(tempItemID)
              console.log('Wert ' + state + ' für ' + tempItemID + ' gesetzt')
            }
          })
        })
      observerWeatherForecastAtAhaus()

      console.log('getWeatherInfos Ende')
    }

  }
}

function putDataFromWeatherAtAhausToHTMLSide (data) {
  var temp = data.main.temp
  document.getElementById('weatherAtAhausTemp').innerHTML = '' + temp
  var tempFeelsLike = data.main.feels_like
  document.getElementById('wetherAtAhausFeelsLike').innerHTML = '' + tempFeelsLike
  var humidity = data.main.humidity
  document.getElementById('weatherAtAhausHumidity').innerHTML = '' + humidity
  var pressure = data.main.pressure
  document.getElementById('weatherAtAhausPressure').innerHTML = '' + pressure
  var weatherData = data.weather[0]
  var weatherIconID = weatherData.icon
  console.log('iconid ' + weatherIconID)
  var weatherIconString = 'http://openweathermap.org/img/wn/' + weatherIconID + '@2x.png'
  document.getElementById('weatherAtAhausIcon').setAttribute('src', weatherIconString)
  var weatherDescription = weatherData.description
  document.getElementById('weatherAtAhausCaption').innerHTML = '' + weatherDescription
  var windSpeed = (data.wind.speed * 3.6).toFixed(2) + ' km/h'
  document.getElementById('weatherAtAhausWindSpeed').innerHTML = '' + windSpeed
  var windDegree = getCorrectDegree(data.wind.deg)
  var styleDegreeRotate = 'transform: rotate(' + windDegree + 'deg);'
  document.getElementById('weatherAtAhausArrow').setAttribute('style', styleDegreeRotate)
  var sunrise = new Date((data.sys.sunrise * 1000)).toLocaleTimeString()
  document.getElementById('sunrise').innerHTML = sunrise
  var sunset = new Date((data.sys.sunset * 1000)).toLocaleTimeString()
  document.getElementById('sunset').innerHTML = sunset
}

function observerWeatherForecastAtAhaus () {
  var docID = 'weatherForecastAtAhaus'
  firebaseApp.firestore().collection('items').doc(docID)
    .onSnapshot(function (doc) {
      if (doc.exists) {
        var changedData = doc.data()
        document.getElementById('forecastCollection').innerHTML = ' <li class="collection-header"><h5>Vorhersage</5></li>'
        changedData.list.forEach(fillForecastData)
        // forecastList=changedData.list
        console.log('update observerForItemWeatherForecastAtAhaus')

        // console.log('Document data:', doc.data())
      } else {
        // doc.data() will be undefined in this case
        console.log('No such document!')
      }
    })
}

function fillForecastData (item, index) {
  var forecastDate = new Date((item.dt * 1000))
  var dayString = getWeekDay(forecastDate)
  var dateTime = '<span>' + dayString + ' ' + forecastDate.toLocaleTimeString() + '</span>'

  var weatherIconUrl = 'http://openweathermap.org/img/wn/' + item.weather[0].icon + '.png'
  var weatherIcon = '<img src="' + weatherIconUrl + '" style="margin-left:0.5em;margin-right:0.5em;"/>'

  var temperature = '<span><i class="fas fa-thermometer-three-quarters" style="margin-right:0.5em;" ></i>' + item.main.temp.toFixed(2) + '</span>'

  var windSpeed = (item.wind.speed * 3.6).toFixed(2)
  var wind = '<span><i class="fas fa-wind" style="margin-left:1.5em;margin-right:0.5em;" ></i>' + windSpeed + '</span>'

  var feelslike = '<span><i class="fas fa-male" style="margin-left:1.5em;"></i><i class="fas fa-thermometer-empty" style="margin-right:0.5em;" ></i>' + item.main.feels_like + '</span>'
  var list = '<li  class="collection-header" style="font-size:x-large;">' + dateTime + '' + weatherIcon + '' + temperature + '' + wind + '' + feelslike + ' </li>'
  document.getElementById('forecastCollection').innerHTML += list
}

function getWeekDay (date) {
  var number = date.getDay()
  var day
  switch (number) {
    case 0:
      day = 'So'
      break
    case 1:
      day = 'Mo'
      break
    case 2:
      day = 'Di'
      break
    case 3:
      day = 'Mi'
      break
    case 4:
      day = 'Do'
      break
    case 5:
      day = 'Fr'
      break
    case 6:
      day = 'Sa'
  }
  return day
}

function observerForItemWeatherAtAhaus () {
  var docID = 'weatherAtAhaus'
  firebaseApp.firestore().collection('items').doc(docID)
    .onSnapshot(function (doc) {
      if (doc.exists) {
        var changedData = doc.data()
        putDataFromWeatherAtAhausToHTMLSide(changedData)
        console.log('update observerForItemWeatherAtAhaus')
        // console.log('Document data:', doc.data())
      } else {
        // doc.data() will be undefined in this case
        console.log('No such document!')
      }
    })
}

function observerForItem (docID) {
  firebaseApp.firestore().collection('items').doc(docID)
    .onSnapshot(function (doc) {
      if (doc.exists) {
        var changedData = doc.data()
        var state = changedData.State
        state = state.replace('?C', '')
        document.getElementById(changedData.ItemID).innerHTML = '' + state
        console.log('update observerForItem')
        // console.log('Document data:', doc.data())
      } else {
        // doc.data() will be undefined in this case
        console.log('No such document!')
      }
    })
}

function getCorrectDegree (inputDegree) {
  if (inputDegree >= 45) {
    return inputDegree - 45
  } else {
    // eslint-disable-next-line no-unused-expressions
    inputDegree + 360 - 45
  }
}
