import firebase from 'firebase'
import firebaseApp from './firebaseInit'

export default {
  name: 'auth',
  mounted () {
    var sourceuri = './authsucess'
    try {
      sourceuri = this.$route.query.sourceuri
    } catch (error) {
      sourceuri = './auhtsucess'
    }

    firebaseApp.auth().onAuthStateChanged((user) => {
      if (user) {
        //console.log('User ist  angemeldet')
        this.$router.push(sourceuri)
      } else {
        var provider = new firebase.auth.GoogleAuthProvider()
        firebase.auth().signInWithRedirect(provider)
        firebase.auth().getRedirectResult().then(function (result) {
          if (result.credential) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken
          }
          // The signed-in user info.
          var user = result.user
          console.log(token + ' user ' + user)
        }).catch(function (error) {
          // Handle Errors here.
          var errorCode = error.code
          var errorMessage = error.message
          console.log(errorCode + 'message ' + errorMessage)
          // The email of the user's account used.
          //  var email = error.email
          // The firebase.auth.AuthCredential type that was used.
          // var credential = error.credential
          // ...
        })
      }
    })
  }

}
