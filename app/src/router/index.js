import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Auth from '@/components/Auth'
import AuthSuccess from '@/components/AuthSuccess'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/auth',
      name: 'Auth',
      component: Auth
    },
    {
      path: '/authsuccess',
      name: 'AuthSuccess',
      component: AuthSuccess
    },
    {
      path: '/devices',
      name: 'Devices',
      component: require('../components/Devices/Devices.vue').default
    },
    {
      path: '/alarm',
      name: 'Alarm',
      component: require('../components/Alarm/Alarm.vue').default
    },
    {
      path: '/weather',
      name: 'Weather',
      component: require('../components/Weather/Weather.vue').default
    },
    {
      path: '/homeauto',
      name: 'Homeautomation',
      component: require('../components/Homeauto/Homeauto.vue').default
    }
  ]
})
