const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require('./credentials.json')
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://schmitttestathome.firebaseio.com"
})

const request = require('request')
var rp = require('request-promise');


const APP_NAME = 'schmitttestathome'


exports.alarmObserver = functions.firestore
  .document('alarms/0THNhs514sx0o1uqACLe')
  .onUpdate((change, context) => {
    const newValue = change.after.data()
    //console.log("new Value" + newValue)
    var alarm = false
    alarm = newValue.alarm
    var deviceID = String(newValue.deviceid)
    //console.log("Device ID = "+deviceID)

    if (alarm) {
      console.log('message  muss gesendet werden')
      sendAlarmMessageForDevice(deviceID)
    } else {
      return console.log('Value ist false es muss keine Mail gesendet werden')
    }
  })

exports.batteryStatusObserverBalkon = functions.firestore
  .document('/jobs/gfhmL2pe7byENna9HiMY')
  .onUpdate((change, context) => {
    const batteryStatusObjectAfterChange = change.after.data()
    const batteryStatusObjectBeforeChange = change.before.data()

    var lastBatteryStatusValueAsInt = extractValue(batteryStatusObjectBeforeChange)
    var newBatteryStatusValueAsInt = extractValue(batteryStatusObjectAfterChange)

    var moduloRest = newBatteryStatusValueAsInt % 5

    if (newBatteryStatusValueAsInt <= 20 && moduloRest === 0 && lastBatteryStatusValueAsInt !== newBatteryStatusValueAsInt) {
      console.log('Batteriestatus  muss gesendet werden')

      var subject = `Die Batterieladung des Balkonsensor beträgt ${newBatteryStatusValueAsInt}!`
      var body = `Hi, der Status der Batterie beträgt ${newBatteryStatusValueAsInt}  %`

      sendMail(subject, body)
    }
    return ''
    //return console.log('durchlaufen')
  })

exports.batteryStatusObserver = functions.firestore
  .document('/jobs/gG0dFaFjVcsES4IX70dt')
  .onUpdate((change, context) => {
    const batteryStatusObjectAfterChange = change.after.data()
    const batteryStatusObjectBeforeChange = change.before.data()

    var lastBatteryStatusValueAsInt = extractValue(batteryStatusObjectBeforeChange)
    var newBatteryStatusValueAsInt = extractValue(batteryStatusObjectAfterChange)

    var moduloRest = newBatteryStatusValueAsInt % 5

    if (newBatteryStatusValueAsInt <= 20 && moduloRest === 0 && lastBatteryStatusValueAsInt !== newBatteryStatusValueAsInt) {
      console.log('Batteriestatus  muss gesendet werden')

      var subject = `Die Batterieladung des Garagensensors beträgt ${newBatteryStatusValueAsInt}!`
      var body = `Hi, der Status der Batterie beträgt ${newBatteryStatusValueAsInt}  %`

      sendMail(subject, body)
    }
    return ''
    //return console.log('durchlaufen')
  })

exports.getWeatherForecastDataScheduled = functions.pubsub.schedule('every 10 minutes').onRun((context) => {

  console.log("Funktion aufgerufen ")
  var options = {
    uri: 'http://api.openweathermap.org/data/2.5/forecast?q=Ahaus&&lang=German&units=metric&APPID=c596f9e68e59b5e570bf26858a346625 ',
    qs: {
      //access_token: 'xxxxx xxxxx' // -> uri + '?access_token=xxxxx%20xxxxx'
    },
    headers: {
      'User-Agent': 'Request-Promise'
    },
    json: true // Automatically parses the JSON string in the response
  }
  console.log('rp options wird aufgerufen')
  return rp(options)
    .then((data) => {
      console.log('Http Request ist zurück. Nun werden die Daten in die Datenbank geschrieben')
      // add an implicit return here
      return admin.firestore().collection(`items`).doc(`weatherForecastAtAhaus`).set(data)
    })
    .then(() => {
      console.log('Daten erfolgreich geschrieben')
      // need's to return something here, using a boolean for simplicity
      return true;
    })
    .catch(function(err) {
      console.log('Fehler ', err)

    })

})

exports.getWeatherDataScheduled = functions.pubsub.schedule('every 10 minutes').onRun((context) => {
  console.log('This will be run every 10 minutes!')
 return request.get('http://api.openweathermap.org/data/2.5/weather?q=Ahaus&units=metric&APPID=c596f9e68e59b5e570bf26858a346625&units=metric&lang=de', function(err, response, body) {
    if (err) {
      console.log('Fehler http Request ' + err)

    } else {
      // console.log(body)
      let data = JSON.parse(body)
      data.ItemID = "weatherAtAhaus"

      return admin.firestore().collection(`items`).doc(`weatherAtAhaus`).set(data)
        .then(() => {
          console.log("END")

          return true
        })
        .catch((error) => {
          console.log("FEHLER  Write Firestore" + error)
        })


    }


  })

})

function extractValue(batteryStatusObject) {
  var batterStatusResultPayload = JSON.parse(batteryStatusObject.resultPayload)
  var newBatteryStatusValueAsString = batterStatusResultPayload.state
  newBatteryStatusValueAsString = newBatteryStatusValueAsString.replace('"', '')
  var newBatteryStatusValueAsInt = parseInt(newBatteryStatusValueAsString)
  return newBatteryStatusValueAsInt

}

function sendAlarmMessageForDevice(deviceID) {

  admin.firestore().collection('devices').doc(deviceID).get()
    .then(function(doc) {
      if (doc.exists) {
        var name = doc.data().name
        console.log("Name des Device " + name)
        var subject = 'Alarm Meldung von Device ' + name
        var body = 'Device ' + name + ' meldet Alarm'
        sendMail(subject, body)
        return name
      } else {
        console.log("Device mit der " + deviceID + " kann nicht gefunden werden")
        return null
      }
    }).catch(function(error) {
      console.log("Fehler in getDeviceNameFromFirestore für " + deviceID + " : " + error)
    })
}

function sendMail(subject, body) {
  admin.firestore().collection('mail').add({
  to: 'franz.schmitt01@gmail.com',
  message: {
    subject: ''+subject,
    text: ''+body,
  }
}).then(() => console.log('Queued email for delivery!'));
}